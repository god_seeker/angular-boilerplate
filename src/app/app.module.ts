import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LayoutComponent } from './layouts/layout.component';
import { SiderComponent } from './layouts/sider/sider.component';
import { ContentComponent } from './layouts/content/content.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';

import { StoreModule } from '@ngrx/store';
import { navReducer } from './reducers/nav.reducer';
import { HeaderComponent } from './layouts/header/header.component';
import { authReducer } from './reducers/auth.reducer';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    SiderComponent,
    ContentComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ navStore: navReducer, authStore: authReducer }, {}),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
