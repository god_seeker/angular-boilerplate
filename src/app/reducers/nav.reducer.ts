import { createReducer, on } from '@ngrx/store';
import { setActive, setMenu } from '../actions/nav.action';

export interface NavStore {
  active: string;
  items: any[];
}

export const initialState: NavStore = {
  active: '',
  items: [],
};

const _navReducer = createReducer(
  initialState,
  on(setMenu, (state, { items }) => ({ ...state, items: items })),
  on(setActive, (state, { active }) => ({ ...state, active: active }))
);

export function navReducer(state: any, action: any) {
  return _navReducer(state, action);
}
