import { createReducer, on } from '@ngrx/store';
import { login, logout } from '../actions/auth.actions';

export interface User {
  name: string;
  role?: string[];
}

export interface AuthStore {
  isLoggedIn: boolean;
  userProfile: User | null;
}

export const initialState: AuthStore = {
  isLoggedIn: false,
  userProfile: null,
};

const _authReducer = createReducer(
  initialState,
  on(login, (state, { username, password }) => {
    const user: User = { name: username };
    return { ...state, isLoggedIn: true, userProfile: user };
  }),
  on(logout, (state) => initialState)
);

export function authReducer(state: any, action: any) {
  return _authReducer(state, action);
}
