import { Component } from '@angular/core';

@Component({
  template: `
    <div class="view-port">
      <app-header id="header"></app-header>
      <div id="row">
        <app-sider id="sider"></app-sider>
        <div id="column">
          <app-content id="content"> </app-content>
          <footer id="footer">Footer</footer>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./layout.css'],
})
export class LayoutComponent {}
