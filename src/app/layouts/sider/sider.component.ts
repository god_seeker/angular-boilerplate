import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { setActive, setMenu } from 'src/app/actions/nav.action';
import { NavStore } from 'src/app/reducers/nav.reducer';

@Component({
  selector: 'app-sider',
  templateUrl: './sider.component.html',
  styleUrls: ['./sider.component.css'],
})
export class SiderComponent implements OnInit {
  menu$: Observable<any[]> = this.store.select((state) => state.navStore.items);

  activeItem?: string;

  constructor(private store: Store<{ navStore: NavStore }>) {
    this.store
      .select('navStore')
      .subscribe((value) => (this.activeItem = value.active));
  }

  ngOnInit(): void {
    this.store.dispatch(setMenu({ items: ['Menu 1', 'Menu 2', 'Mune 3'] }));
    this.store.dispatch(setActive({ active: 'Menu 1' }));
  }

  setActive(m: any) {
    this.store.dispatch(setActive({ active: m }));
  }
}
