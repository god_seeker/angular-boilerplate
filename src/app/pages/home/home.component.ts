import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { NavStore } from 'src/app/reducers/nav.reducer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  activeMenu?: string;

  constructor(private store: Store<{ navStore: NavStore }>) {
    this.store.select('navStore').subscribe((value) => {
      this.activeMenu = value.active;
    });
  }

  ngOnInit(): void {}
}
