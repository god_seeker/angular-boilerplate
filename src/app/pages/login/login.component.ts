import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { login, logout } from 'src/app/actions/auth.actions';
import { AuthStore } from 'src/app/reducers/auth.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private store: Store<{ authStore: AuthStore }>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.store.dispatch(logout());

    this.store
      .select('authStore')
      .subscribe((auth: AuthStore) =>
        auth.isLoggedIn ? this.router.navigate(['']) : null
      );
  }

  onLogin(): void {
    const user = { username: 'John', password: 'John' };
    this.store.dispatch(login(user));
  }
}
