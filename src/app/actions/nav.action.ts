import { createAction, props } from '@ngrx/store';

export const setMenu = createAction(
  '[Naviation] setMenu',
  props<{ items: any[] }>()
);
export const setActive = createAction(
  '[Navigation] setActive',
  props<{ active: string }>()
);
