import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Authentication] Login',
  props<{ username: string; password: string }>()
);

export const logout = createAction('[Authentication] Logout');
